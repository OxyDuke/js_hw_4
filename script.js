// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати.
// Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.

function setNumbers() {
  let num1 = prompt("Please write the first number");
  let num2 = prompt("Please write the second number");
  let operation = prompt("Please write what operation to make");
  setNumbers_validator(num1, num2, operation);
}

function resultNum(num1, num2, operation) {
    let res = 0;
    switch (operation) {
      case "+":
        res = Number(num1) + Number(num2);
        console.log(res);
        break;
  
      case "-":
        res = Number(num1) - Number(num2);
        console.log(res);
        break;
  
      case "*":
        res = Number(num1) * Number(num2);
        console.log(res);
        break;
  
      case "/":
        res = Number(num1) / Number(num2);
        console.log(res);
        break;
    }
}

setNumbers();

// Необов'язкове завдання підвищеної складності
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа,
// або при вводі вказав не числа, - запитати обидва числа знову
// (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).

function setNumbers_validator(num1, num2, operation) {
  let key = true;
  while (key) {
    if (isNaN(num1) || num1 === "" || num1 === " ") {
      num1 = prompt("Incorrect argument \nPlease write the first number");
    } else if (isNaN(num2) || num2 === "" || num2 === " ") {
      num2 = prompt("Incorrect argument \nPlease write the second number");
    } else if (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/") {
      operation = prompt("Incorrect operation \nPlease write what operation to make");
    } else {
      key = false;
    }
  }
  resultNum(num1, num2, operation);
}


